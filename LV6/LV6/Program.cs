﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6
{
    class Program
    {
        static void Main(string[] args)
        {

            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("Note", "Note description"));
            notebook.AddNote(new Note("Note2", "Empty note"));
            notebook.AddNote(new Note("Note3", "Its free"));

            Iterator iterator = new Iterator(notebook);
            

            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
