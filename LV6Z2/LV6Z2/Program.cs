﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6Z2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();
            box.AddProduct(new Product("Prod1", 100));
            box.AddProduct(new Product("Prod2", 200));
            box.AddProduct(new Product("Prod3", 300));


            Iterator boxiterator = new Iterator(box);
            boxiterator.Current.ToString();
            boxiterator.Next().ToString();

            while (boxiterator.IsDone != true)
            {
                boxiterator.Current.ToString();
                boxiterator.Next();
            }
        }
    }
}
